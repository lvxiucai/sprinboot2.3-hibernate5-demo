# 快速开始
本项目使用了最新的springboot2.3+hibernate5作为基础框架引擎，并配置了hikari连接池的配置（经生产验证过的）

在项目使用过程中如遇到问题，可添加秀才微信18306482797，有私活也可以找我噢


#本地启动
修改application.yml中的环境DEPLOY_ENV为dev，然后修改application-dev.yml中的数据库配置
HibernateDemoApplication====> run/debug 启动即可~~~



# sprinboot2.3-hibernate5-demo

#### 介绍
2020年10月份搭建的最新的springboot2.x+hibernate5的demo工程，
主要是给想使用hibernate改造老项目或者新项目启动的一个最简版的脚手架工程，
省去一部分时间快速改造项目

#### 软件架构
本项目是基于老项目使用的HibernateTemplate做查询，其实也可以使用repository那种，看各自的需求了

