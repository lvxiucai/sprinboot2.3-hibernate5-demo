package com.example.hibernate.demo.pojo;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @author lvxiucai
 * @description
 * @date 2020/10/22
 */
@Entity
@Table(name = "stu")
public class Student implements Serializable{

    @Id
    @Column(name = "s_id",unique = true,nullable = false)
    private Long id;
    @Column(name = "s_name")
    private String name;
    @Column(name = "score")
    private Integer score;
    @Column(name = "sex")
    private Integer sex;

    public Student() {
    }

    public Student(Long id, String name, Integer score, Integer sex) {
        this.id = id;
        this.name = name;
        this.score = score;
        this.sex = sex;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    public Integer getSex() {
        return sex;
    }

    public void setSex(Integer sex) {
        this.sex = sex;
    }
}
