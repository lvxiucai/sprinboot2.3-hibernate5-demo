package com.example.hibernate.demo.controller;

import com.example.hibernate.demo.pojo.Student;
import com.example.hibernate.demo.service.IDemoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * @author lvxiucai
 * @description
 * @date 2020/10/22
 */
@RestController
public class DemoController {


    @Autowired
    private IDemoService demoService;

    @RequestMapping("/saveStu")
    public Map<String,Object> saveStudent(@RequestBody Student stu){
        Map<String,Object> result = new HashMap<>();
        demoService.saveStu(stu);
        result.put("success",true);
        return result;
    }



}
