package com.example.hibernate.demo.service;

import com.example.hibernate.demo.pojo.Student;
import com.example.hibernate.demo.util.IdUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate5.HibernateTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

/**
 * @author lvxiucai
 * @description
 * @date 2020/10/22
 */
@Service
public class DemoServiceImpl implements IDemoService{


    @Autowired
    private HibernateTemplate hibernateTemplate;


    @Override
    @Transactional(rollbackFor = Exception.class)
    public void saveStu(Student student) {
        if(student.getId()==null||student.getId()==0L){
            student.setId(Long.parseLong(IdUtils.nextId()));
        }
        hibernateTemplate.saveOrUpdate(student);
    }
}
