package com.example.hibernate.demo.service;

import com.example.hibernate.demo.pojo.Student;
import org.springframework.stereotype.Repository;

/**
 * @author lvxiucai
 * @description
 * @date 2020/10/22
 */
@Repository
public interface IDemoService {

    void saveStu(Student student);
}
